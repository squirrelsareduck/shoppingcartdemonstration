Getting Started with Bootstrap:

getbootstrap.com



Some bootstrap templates that people have already created (free to use):

bootsnipp.com


How to use in a simple web server:

Copy this HTML code into yours.  Run your server.  Enjoy.

You will need to handle the HTTP POST request on your server when the user
submits the form.  The implementation of this depends on your particular server's
runtime environment.



This code is based off of:

https://bootsnipp.com/snippets/featured/shopping-cart-bs-3

Licensed under the MIT license, as per bootsnipp's terms.







Snippets License (MIT license)

Copyright (c) 2013 Bootsnipp.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
